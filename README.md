# Test Task description

### Task Definition

Напишите склад и интеграционный тест-имитатор его работы
На складе имеются различные товары. Товар характеризуется
наименованием, весом и ценой. Клиенты могут в любой момент заказать товар
прямо со склада, если он имеется. Приложением должно иметь RESTful API
Интеграционный тест:
1. Начальный набор товаров генерируется скриптом
2. Создается N потоков-потребителей. Каждый потребитель может
разместить заказ на выкуп части товаров. То есть в заказе указан список
артикулов товаров и требуемое количество
Технологии
При написании приложения можно использовать такие технологии:
- Любая RDBMS (кроме проприетарных)
- Spring framework
- ORM Hibernate
Будет плюсом, если приложение будет докеризуемо (использование
технологии docker)

### Prerequisites
You need installed docker daemon on the machine you are going to run test task
* The test configuration creates/runs docker container on local host 
* The test connects to the exposed port 8080 on localhost   

### Instruction to run
To run test task:
* clone the repo
* run commands
    * Build the image: ```./gradlew clean :wh-server:bootBuildImage```
    * Execute end to end test: ```./gradlew :wh-e2etest:check -i```

#### Author
[Sergii Tsymbaliuk](mailto:sergii@tsymbaliuk.net)



