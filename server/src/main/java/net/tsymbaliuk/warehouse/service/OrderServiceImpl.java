package net.tsymbaliuk.warehouse.service;

import net.tsymbaliuk.warehouse.data.GoodsRepo;
import net.tsymbaliuk.warehouse.data.OrdersRepo;
import net.tsymbaliuk.warehouse.pojos.db.Goods;
import net.tsymbaliuk.warehouse.pojos.db.Order;
import net.tsymbaliuk.warehouse.pojos.db.OrderItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.transaction.annotation.Isolation.REPEATABLE_READ;

@Service
public class OrderServiceImpl implements OrderService{
    private static final Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    GoodsRepo goodsRepo;

    @Autowired
    OrdersRepo ordersRepo;

    @Transactional(isolation = REPEATABLE_READ, rollbackFor=Exception.class)
    public Long postOrder(List<OrderItem> orderItems) throws NotEnoughAmountException, GoodsNotFoundException {
        log.info("Receive order for {}", Arrays.toString(orderItems.toArray()));
        Map<Long, Double> orderItemsMap = orderItems.stream().
                collect(Collectors.toMap(
                        OrderItem::getGoodsVendorCode,
                        OrderItem::getAmount));

        Iterable<Goods> goodsCollection = goodsRepo.findAllById(orderItemsMap.keySet());

        for (Goods goods : goodsCollection) {
            Long k = goods.getVendorCode();
            Double v = orderItemsMap.get(k);

            log.info("Checking available amount for goods {}, need {}, has {}", k, v, goods.getCount());

            // check available amount
            Double newCount = goods.subCount(v);
            if (newCount >= 0) {
                goods = goodsRepo.save(goods);
                log.info("Updated count for {} to {}", goods , newCount);
            } else {
                log.error(String.format("Not enough goods %s, has only %f of %f", goods, goods.getCount(), v));
                throw new NotEnoughAmountException(
                        String.format("Not enough goods %s '%s', has only %f of %f",
                                goods.getVendorCode(), goods.getName(), goods.getCount(), v));
            }
            orderItemsMap.remove(k);
        }

        // check all items exists in store
        if (!orderItemsMap.isEmpty()) {
            log.error("Didn't find all good in repo: {}", orderItemsMap);
            throw new GoodsNotFoundException("Cannot find all Goods for order " + orderItemsMap);
        }

        final Long orderNumber = ordersRepo.save(new Order(orderItems)).getOrderNumber();
        log.info("Created order {}", orderNumber);
        return orderNumber;
    }
}
