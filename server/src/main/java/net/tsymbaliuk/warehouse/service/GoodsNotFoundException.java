package net.tsymbaliuk.warehouse.service;

public class GoodsNotFoundException extends Exception {

    public GoodsNotFoundException() {
        super();
    }

    public GoodsNotFoundException(String message) {
        super(message);
    }

    public GoodsNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public GoodsNotFoundException(Throwable cause) {
        super(cause);
    }

    protected GoodsNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
