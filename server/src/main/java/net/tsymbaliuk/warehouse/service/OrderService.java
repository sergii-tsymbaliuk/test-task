package net.tsymbaliuk.warehouse.service;

import net.tsymbaliuk.warehouse.pojos.db.OrderItem;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface OrderService {
    public Long postOrder(List<OrderItem> orderItems) throws NotEnoughAmountException, GoodsNotFoundException;
}
