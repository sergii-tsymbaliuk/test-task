package net.tsymbaliuk.warehouse.api;

import net.tsymbaliuk.warehouse.data.GoodsRepo;
import net.tsymbaliuk.warehouse.pojos.db.Goods;
import net.tsymbaliuk.warehouse.service.GoodsNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class WarehouseController {
    private static final Logger log = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private GoodsRepo goodsRepo;

    @GetMapping("/goods")
    public List<Goods> getAllGoods() {
        ArrayList<Goods> res = new ArrayList<>();
        goodsRepo.findAll().forEach(res::add);
        return res;
    }

    @GetMapping(path="/goods/{vendorCode}")
    public Goods getGoodsByVendorCode(@PathVariable("vendorCode") Long vendorCode) throws GoodsNotFoundException {
        return goodsRepo.findById(vendorCode).orElseThrow(GoodsNotFoundException::new);
    }
}
