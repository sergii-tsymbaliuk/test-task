package net.tsymbaliuk.warehouse.api;

import net.tsymbaliuk.warehouse.pojos.api.CreateOrderRequest;
import net.tsymbaliuk.warehouse.pojos.api.CreateOrderResponse;
import net.tsymbaliuk.warehouse.pojos.db.OrderItem;
import net.tsymbaliuk.warehouse.service.GoodsNotFoundException;
import net.tsymbaliuk.warehouse.service.NotEnoughAmountException;
import net.tsymbaliuk.warehouse.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class OrderController {
    private static final Logger log = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @PostMapping(path="/order", produces = "application/json")
    public ResponseEntity<?> postOrder(@RequestBody CreateOrderRequest createOrderRequest) throws NotEnoughAmountException {
        ResponseEntity<?> resp;
        List<OrderItem> orderItems = createOrderRequest.getOrderItems();
        log.info("Received post order for {}", Arrays.toString(orderItems.toArray()));
        try {
            final Long orderNumber = orderService.postOrder(orderItems);
            log.info("Created order number {}", orderNumber);
            resp = new ResponseEntity<>(new CreateOrderResponse(orderNumber), HttpStatus.CREATED);
        } catch (NotEnoughAmountException e) {
            resp = new ResponseEntity<>(e.toString(), HttpStatus.BAD_REQUEST);
        } catch (GoodsNotFoundException e) {
            resp = new ResponseEntity<>(e.toString(), HttpStatus.NOT_FOUND);
        } catch (TransactionException e) {
            resp = new ResponseEntity<>(e.toString(), HttpStatus.CONFLICT);
        } catch (Exception e) {
            resp = new ResponseEntity<>(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return resp;
    }

    @GetMapping(path="/orderRequest", produces = "application/json")
    public CreateOrderRequest getOrderRequest() {
        CreateOrderRequest req = new CreateOrderRequest();

        for (Long i = 1L; i< 5; i ++) {
            req.addItemToOrder(i, i * 1.00);
        }

        return req;
    }
}