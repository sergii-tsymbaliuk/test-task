package net.tsymbaliuk.warehouse;

import net.tsymbaliuk.warehouse.data.GoodsRepo;
import net.tsymbaliuk.warehouse.pojos.db.Goods;
import net.tsymbaliuk.warehouse.service.GoodsNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(exclude = {ErrorMvcAutoConfiguration.class})
public class Application {
    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner demo(GoodsRepo repository) {
        return (args) -> {
            // save a few customers
            repository.save(new Goods("PepperJack", 10.0, 3.0, 100.0));
            repository.save(new Goods("Philadelphia", 5.0, 5.0, 230.0));
            repository.save(new Goods("WhitePotato", 1.0, 1.0, 500.0));
            repository.save(new Goods("Cheddar", 10.0, 3.0, 100.0));
            repository.save(new Goods("Olive Oil", 10.0, 3.0, 100.0));

            // fetch all customers
            log.info("Goods found with findAll():");
            log.info("-------------------------------");
            for (final Goods goods : repository.findAll()) {
                log.info(goods.toString());
            }
            log.info("");

            // fetch an individual customer by ID
            final Goods goods = repository.findById(1L).orElseThrow(GoodsNotFoundException::new);
            log.info("Goods found with findById(1L):");
            log.info("--------------------------------");
            log.info(goods.toString());
            log.info("");

            log.info("");
        };
    }
}
