package net.tsymbaliuk.warehouse.data;

import net.tsymbaliuk.warehouse.pojos.db.Goods;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.LockModeType;
import java.util.Optional;

public interface GoodsRepo extends CrudRepository<Goods, Long> {
    @Override
    @Lock(LockModeType.PESSIMISTIC_READ)
    Optional<Goods> findById(Long id);

    @Override
    @Lock(LockModeType.PESSIMISTIC_READ)
    Iterable<Goods> findAllById(Iterable<Long> ids);
}
