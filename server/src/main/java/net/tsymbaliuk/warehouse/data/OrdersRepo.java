package net.tsymbaliuk.warehouse.data;

import net.tsymbaliuk.warehouse.pojos.db.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrdersRepo extends CrudRepository<Order, Long> {
    List<Order> findByOrderNumber(Long orderNumber);
}
