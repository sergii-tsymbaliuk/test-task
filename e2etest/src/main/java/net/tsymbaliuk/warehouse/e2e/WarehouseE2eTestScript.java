package net.tsymbaliuk.warehouse.e2e;

import com.google.gson.Gson;
import net.tsymbaliuk.warehouse.pojos.api.CreateOrderRequest;
import net.tsymbaliuk.warehouse.pojos.api.CreateOrderResponse;
import net.tsymbaliuk.warehouse.pojos.db.Goods;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.NoConnectionReuseStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class WarehouseE2eTestScript {
    private static final Logger log = LoggerFactory.getLogger(WarehouseE2eTestScript.class);

    private static final String imageName = "docker.io/library/wh-server:0.0.1-SNAPSHOT";
    private static final String port = "8080";
    private static final int numberOfThreads = 10;

    private static final Gson gson = new Gson();

    public static void main(String [] args) {
        try {
            new WarehouseE2eTestScript().testOrderApi();
        } catch (InterruptedException | IOException e) {
            log.error("Test failed due to exception", e);
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void testOrderApi() throws InterruptedException, IOException {

        Goods goodsOne = getGoods(1L);
        Goods goodsTwo = getGoods(2L);
        Goods goodsThree = getGoods(3L);

        Map<Long, Double> orderItems = Map.of(
                1L, 5.0,
                2L, 7.0,
                3L, 2.0);

        ExecutorService service = Executors.newFixedThreadPool(numberOfThreads);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);

        AtomicInteger orderCounter = new AtomicInteger();
        for (int i = 0; i < numberOfThreads; i++) {
            service.execute(() -> {
                try (CloseableHttpClient httpClient = getHttpClient()) {
                    Long orderNumber = postOrder(orderItems, httpClient);
                    if (orderNumber > 0L) {
                        orderCounter.getAndIncrement();
                    } else {
                        System.out.println("Post order failed");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                latch.countDown();
            });
        }
        latch.await();
        service.shutdown();
        checkEquals(orderCounter.get(), numberOfThreads, "all orders processed");

        checkEquals(getGoods(1L).getCount(),
                goodsOne.getCount() - numberOfThreads * 5.0,
                "goods #1 changed properly " + numberOfThreads + " times");

        checkEquals(getGoods(2L).getCount(),
                goodsTwo.getCount() - numberOfThreads * 7.0,
                "goods #2 changed properly " + numberOfThreads + " times");

        checkEquals(getGoods(3L).getCount(),
                goodsThree.getCount() - numberOfThreads * 2.0,
                "goods #3 changed properly " + numberOfThreads + " times");
    }

    private void checkEquals(Double actual, Double expected, String message) {
        if ((long)(actual * 1000) == (long)(expected * 1000)) {
            log.info("Check {}: PASSED", message);
        } else {
            log.error("Check {}: FAILED, expected {} but was {}", message, expected, actual);
//            System.exit(1);
        }
    }

    private void checkEquals(Integer actual, Integer expected, String message) {
        if ((long)(actual * 1000) == (long)(expected * 1000)) {
            log.info("Check {}: PASSED", message);
        } else {
            log.error("Check {}: FAILED, expected {} but was {}", message, expected, actual);
//            System.exit(1);
        }
    }

    protected Long postOrder(Map<Long, Double> orderItems, CloseableHttpClient httpClient) {
        HttpPost postRequest = new HttpPost("http://localhost:8080/order");
        postRequest.addHeader("Content-Type", "application/json");
        CreateOrderRequest request = new CreateOrderRequest();
        orderItems.forEach(request::addItemToOrder);
        HttpEntity entity = null;
        try {
            entity = new ByteArrayEntity(gson.toJson(request).getBytes("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        postRequest.setEntity(entity);
        try (CloseableHttpResponse response = httpClient.execute(postRequest)) {
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED) {
                CreateOrderResponse orderResp = gson.fromJson(
                        new InputStreamReader(response.getEntity().getContent(), "UTF-8"),
                        CreateOrderResponse.class);
                return orderResp.getOrderNumber();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1L;
    }

    protected Goods getGoods(Long vendorCode) throws IOException {
        String urlString = String.format("http://localhost:8080/goods/%d", vendorCode);
        try (CloseableHttpClient client = getHttpClient();
             CloseableHttpResponse response = httpGet(urlString, client)){
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                Goods goods = gson.fromJson(
                        new InputStreamReader(response.getEntity().getContent(), "UTF-8"),
                        Goods.class);
                return goods;
            }
        }
        return null;
    }

//    protected static void startContainer() throws DockerException, InterruptedException {
//        final Map<String, List<PortBinding>> portBindings = Map.of(
//                "8080", List.of(PortBinding.of("0.0.0.0", port)));
//
//        // Create container with exposed ports
//        final ContainerConfig containerConfig = ContainerConfig.builder()
//                .hostConfig(HostConfig.builder().portBindings(portBindings).build())
//                .image(imageName)
//                .exposedPorts(Set.of(port))
//                .build();
//
//        final ContainerCreation creation = docker.createContainer(containerConfig);
//        containerId = creation.id();
//
//        // Start container
//        docker.startContainer(containerId);
//    }
//
//    private static boolean waitBootToStart() throws InterruptedException {
//        long deadline = System.currentTimeMillis() + 10000; // 5 sec
//        while (System.currentTimeMillis() < deadline) {
//            try (CloseableHttpClient httpClient = getHttpClient()){
//                HttpGet httpRequest = new HttpGet("http://localhost:8080/");
//                httpRequest.addHeader("Content-Type", "application/json");
//                try (CloseableHttpResponse httpResp = httpClient.execute(httpRequest)) {
//                    if (httpResp.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_FOUND) {
//                        return true;
//                    }
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            Thread.sleep(300);
//        }
//        return false;
//    }

    private static CloseableHttpResponse httpGet(final String url,
                                                 CloseableHttpClient httpClient) throws IOException {
        HttpGet httpRequest = new HttpGet(url);
        httpRequest.addHeader("Content-Type", "application/json");

        return httpClient.execute(httpRequest);
    }

    private static CloseableHttpClient getHttpClient() {
        return HttpClients.custom()
                .setRetryHandler(new DefaultHttpRequestRetryHandler(3, false))
                .setConnectionReuseStrategy(NoConnectionReuseStrategy.INSTANCE)
                .build();
    }
}
