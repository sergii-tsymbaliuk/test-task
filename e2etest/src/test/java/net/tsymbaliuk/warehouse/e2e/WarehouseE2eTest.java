package net.tsymbaliuk.warehouse.e2e;

import com.google.gson.Gson;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.exceptions.DockerCertificateException;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.HostConfig;
import com.spotify.docker.client.messages.PortBinding;
import net.tsymbaliuk.warehouse.pojos.api.CreateOrderRequest;
import net.tsymbaliuk.warehouse.pojos.api.CreateOrderResponse;
import net.tsymbaliuk.warehouse.pojos.db.Goods;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.NoConnectionReuseStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class WarehouseE2eTest {

    private static final String imageName = "docker.io/library/wh-server:0.0.1-SNAPSHOT";
    private static final String port = "8080";

    private static final Gson gson = new Gson();

    protected static String containerId;
    protected static DockerClient docker;
    protected static LogStream logs;

    @BeforeAll
    public static void setUp() throws DockerException, InterruptedException, DockerCertificateException {
        docker = DefaultDockerClient.fromEnv().build();

        startContainer();
        if (!waitBootToStart()) {
            throw new DockerException("Cannot start container");
        }
        // print container logs
        logs = docker.logs(containerId,
                DockerClient.LogsParam.follow(),
                DockerClient.LogsParam.timestamps(),
                DockerClient.LogsParam.stdout(),
                DockerClient.LogsParam.stderr());
    }

    @AfterAll
    public static void tearDown() throws DockerException, InterruptedException {
        // Kill container
        docker.killContainer(containerId);

        // print logs
        logs.forEachRemaining(lm -> System.out.println(
                StandardCharsets.UTF_8.decode(lm.content()).toString()));

        // Remove container
        docker.removeContainer(containerId);

        // Close the docker client
        docker.close();
    }

    @RepeatedTest(2)
    public void testOrderApi() throws InterruptedException, IOException {

        Goods goodsOne = getGoods(1L);
        Goods goodsTwo = getGoods(2L);
        Goods goodsThree = getGoods(3L);

        Map<Long, Double> orderItems = Map.of(
                1L, 5.0,
                2L, 7.0,
                3L, 2.0);

        int numberOfThreads = 10;
        ExecutorService service = Executors.newFixedThreadPool(numberOfThreads);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);

        AtomicInteger orderCounter = new AtomicInteger();
        for (int i = 0; i < numberOfThreads; i++) {
            service.execute(() -> {
                try (CloseableHttpClient httpClient = getHttpClient()) {
                    Long orderNumber = postOrder(orderItems, httpClient);
                    if (orderNumber > 0L) {
                        orderCounter.getAndIncrement();
                    } else {
                        System.out.println("Post order failed");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                latch.countDown();
            });
        }
        latch.await();

        assertThat(orderCounter.get(), is(numberOfThreads));

        assertThat(getGoods(1L).getCount(),
                is(goodsOne.getCount() - numberOfThreads * 5.0));

        assertThat(getGoods(2L).getCount(),
                is(goodsTwo.getCount() - numberOfThreads * 7.0));

        assertThat(getGoods(3L).getCount(),
                is(goodsThree.getCount() - numberOfThreads * 2.0));
    }

    protected Long postOrder(Map<Long, Double> orderItems, CloseableHttpClient httpClient) {
        HttpPost postRequest = new HttpPost("http://localhost:8080/order");
        postRequest.addHeader("Content-Type", "application/json");
        CreateOrderRequest request = new CreateOrderRequest();
        orderItems.forEach(request::addItemToOrder);
        HttpEntity entity = null;
        try {
            entity = new ByteArrayEntity(gson.toJson(request).getBytes("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        postRequest.setEntity(entity);
        try (CloseableHttpResponse response = httpClient.execute(postRequest)) {
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED) {
                CreateOrderResponse orderResp = gson.fromJson(
                        new InputStreamReader(response.getEntity().getContent(), "UTF-8"),
                        CreateOrderResponse.class);
                return orderResp.getOrderNumber();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1L;
    }

    protected Goods getGoods(Long vendorCode) throws IOException {
        String urlString = String.format("http://localhost:8080/goods/%d", vendorCode);
        try (CloseableHttpClient client = getHttpClient();
             CloseableHttpResponse response = httpGet(urlString, client)){
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                Goods goods = gson.fromJson(
                        new InputStreamReader(response.getEntity().getContent(), "UTF-8"),
                        Goods.class);
                return goods;
            }
        }
        return null;
    }

    protected static void startContainer() throws DockerException, InterruptedException {
        final Map<String, List<PortBinding>> portBindings = Map.of(
                "8080", List.of(PortBinding.of("0.0.0.0", port)));

        // Create container with exposed ports
        final ContainerConfig containerConfig = ContainerConfig.builder()
                .hostConfig(HostConfig.builder().portBindings(portBindings).build())
                .image(imageName)
                .exposedPorts(Set.of(port))
                .build();

        final ContainerCreation creation = docker.createContainer(containerConfig);
        containerId = creation.id();

        // Start container
        docker.startContainer(containerId);
    }

    private static boolean waitBootToStart() throws InterruptedException {
        long deadline = System.currentTimeMillis() + 10000; // 5 sec
        while (System.currentTimeMillis() < deadline) {
            try (CloseableHttpClient httpClient = getHttpClient()){
                HttpGet httpRequest = new HttpGet("http://localhost:8080/");
                httpRequest.addHeader("Content-Type", "application/json");
                try (CloseableHttpResponse httpResp = httpClient.execute(httpRequest)) {
                    if (httpResp.getStatusLine().getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                        return true;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            Thread.sleep(300);
        }
        return false;
    }

    private static CloseableHttpResponse httpGet(final String url,
                                                 CloseableHttpClient httpClient) throws IOException {
        HttpGet httpRequest = new HttpGet(url);
        httpRequest.addHeader("Content-Type", "application/json");

        return httpClient.execute(httpRequest);
    }

    private static CloseableHttpClient getHttpClient() {
        return HttpClients.custom()
                .setRetryHandler(new DefaultHttpRequestRetryHandler(3, false))
                .setConnectionReuseStrategy(NoConnectionReuseStrategy.INSTANCE)
                .build();
    }
}
