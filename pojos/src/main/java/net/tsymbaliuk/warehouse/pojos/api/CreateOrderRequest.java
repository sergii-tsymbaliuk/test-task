package net.tsymbaliuk.warehouse.pojos.api;

import net.tsymbaliuk.warehouse.pojos.db.OrderItem;

import java.util.ArrayList;
import java.util.List;

public class CreateOrderRequest {

    private List<OrderItem> orderItems;

    public CreateOrderRequest() {
        this.orderItems = new ArrayList<>();
    }

    public void addItemToOrder(Long vendorCode, Double count) {
        orderItems.add(new OrderItem(vendorCode, count));
    }

    public List<OrderItem> getOrderItems() {
        return List.copyOf(orderItems);
    }
}
