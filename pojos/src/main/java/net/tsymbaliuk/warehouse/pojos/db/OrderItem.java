package net.tsymbaliuk.warehouse.pojos.db;

import javax.persistence.Embeddable;

@Embeddable
public class OrderItem {
    private Long goodsVendorCode;
    private Double amount;

    public OrderItem() {
    }

    public OrderItem(Long goodsVendorCode, Double amount) {
        this.goodsVendorCode = goodsVendorCode;
        this.amount = amount;
    }

    public Long getGoodsVendorCode() {
        return goodsVendorCode;
    }

    public Double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "goodsVendorCode=" + goodsVendorCode +
                ", amount=" + amount +
                '}';
    }
}
