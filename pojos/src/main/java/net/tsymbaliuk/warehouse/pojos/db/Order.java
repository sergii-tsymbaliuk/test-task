package net.tsymbaliuk.warehouse.pojos.db;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "warehouseOrders")
public class Order {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long orderNumber;

    @Embedded
    List<OrderItem> orderItems = new ArrayList<>();

    public Order() {
    }

    public Order(List<OrderItem> orderItems) {
        this.orderItems.addAll(orderItems);
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public List<OrderItem> getOrderItems() {
        return List.copyOf(orderItems);
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderNumber=" + orderNumber +
                ", orderItems=" + orderItems +
                '}';
    }
}
