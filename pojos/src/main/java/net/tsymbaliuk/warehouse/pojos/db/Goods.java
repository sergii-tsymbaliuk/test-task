package net.tsymbaliuk.warehouse.pojos.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity()
public class Goods {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long vendorCode;

    private String name;
    private Double wight;
    private Double price;

    private Double count;

    public Goods() {
    }

    public Goods(String name, Double wight, Double price, Double count) {
        this.name = name;
        this.wight = wight;
        this.price = price;
        this.count = count;
    }


    public Long getVendorCode() {
        return vendorCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getWight() {
        return wight;
    }

    public void setWight(Double wight) {
        this.wight = wight;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getCount() {
        return count;
    }

    public void setCount(Double count) {
        this.count = count;
    }

    public Double addCount(Double change) {
        count += change;
        return count;
    }

    public Double subCount(Double change) {
        count -= change;
        return count;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "vendorCode=" + vendorCode +
                ", name='" + name + '\'' +
                ", wight=" + wight +
                ", price=" + price +
                ", count=" + count +
                '}';
    }
}
